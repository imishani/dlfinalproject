# Learning Wire Configurations for Real-Time Shape Estimation

---

### Authors: 
### Itamar Mishani (imishani@andrew.cmu.edu)
### Noam Nahum (nahman92@gmail.com)

---

## Data:

First, as describe in the work, we needed to perform a parametric fitting to the data. 
To see an example of how we did it go to <a href="https://gitlab.com/imishani/dlfinalproject/-/blob/main/parametric_fitting.ipynb" target="_blank">parametric_fitting.ipynb</a>

Later, we preprocessed the data. To see this process got to: <a href="https://gitlab.com/imishani/dlfinalproject/-/blob/main/preprocess.ipynb" target="_blank">preprocess.ipynb</a>


## Invertible Neural Network attempts:
To see the notebook with our attempts to learn the map between latent space and F/T measurements space, go to: <a href="https://gitlab.com/imishani/dlfinalproject/-/blob/main/INN.ipynb" target="_blank">INN.ipynb</a>

## Models Architecture:

All models architecture is in the python file: <a href="https://gitlab.com/imishani/dlfinalproject/-/blob/main/AE.py" target="_blank">AE.py</a>

## Training and testing:

In the <a href="https://gitlab.com/imishani/dlfinalproject/-/blob/main/run.ipynb" target="_blank">run.ipynb</a>
you can see all our training (of the various models) and all testings. Here, we also trained a VAE and used it to create our latent space data. 

## Some figures as teaser:
<img src="./data/SAEC_plots.png" alt="teaser" title="Wire shape estimation - inference time">
<!-- ![teaser](href="https://gitlab.com/imishani/dlfinalproject/-/blob/main/data/SAEC_plots.png") -->
